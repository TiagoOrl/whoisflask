from flask import Flask, request, url_for, render_template
from whois import get_dados_html, obter_lista_de_site

import requests



app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")


    

@app.route('/resultado', methods = ['POST'])
def resultado():
    entrada = request.form['input_dominio']
    dados = get_dados_html(entrada)

    return render_template("resultado.html", dados_do_site = dados)


@app.route('/auto_get', methods = ['POST'])
def autoGet():

        json_dados = obter_lista_de_site("/home/tiago_jesus/Área de Trabalho/whoisflask/data.json")
        return render_template("resultado.html", dados_do_site = json_dados)
    



if __name__ == '__main__':
    app.run(debug = True)