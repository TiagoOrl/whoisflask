
import subprocess
import urllib.request, json
from urllib.parse import urlparse



# print(urlparse('http://www.cachorrodoido.com.br/teste.php?nome=teste').netloc)




def get_dados_html(dominio):


    dominio = urlparse(dominio).netloc


    dados = subprocess.check_output("whois " + dominio, shell = True)
    return dados.decode(encoding='UTF-8',errors='ignore')


def baixar_json():

    with urllib.request.urlopen("http://data.phishtank.com/data/online-valid.json") as lista_json:
        dados_json = json.loads(lista_json.read().decode())

    with open('data.json', 'w') as f:
        json.dump(dados_json, f, ensure_ascii=False)




def obter_lista_de_site(arquivo):
      
        i = 0

        with open(arquivo, "r+") as f_dados:
            dados = json.load(f_dados)

        l_dados = ""

        for x in dados:
            
            if i > 40:
                
                break
            else:

                l_dados = l_dados + get_dados_html(x["url"])
                i = i + 1
            
            return l_dados
            
            


# obter_lista_de_site("/home/tiago_jesus/Área de Trabalho/whoisflask/data.json")